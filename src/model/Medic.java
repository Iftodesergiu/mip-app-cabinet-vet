package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medic database table.
 * 
 */
@Entity
@NamedQuery(name="Medic.findAll", query="SELECT m FROM Medic m")
public class Medic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idMedic;

	private String name;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="medic")
	private List<Programare> programares;

	public Medic() {
	}
	public Medic(int idMedic,String Name) {
		this.idMedic=idMedic;
		this.name=Name;
	}
	//CREATE
	public void Create(int idMedic,String Name) {
		this.idMedic=idMedic;
		this.name=Name;
	}
	
	public int getIdMedic() {
		return this.idMedic;
	}

	public void setIdMedic(int idMedic) {
		this.idMedic = idMedic;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setMedic(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setMedic(null);

		return programare;
	}

}