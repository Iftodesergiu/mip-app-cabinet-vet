package model;

import java.io.Serializable;
import javax.persistence.*;
//import java.sql.Time;
//import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProgramare;

	//@Temporal(TemporalType.DATE)
	private String date;

	private String hour;

	private String type;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	//bi-directional many-to-one association to Medic
	@ManyToOne
	@JoinColumn(name="idMedic")
	private Medic medic;

	public Programare() {
	}
	public Programare(int idProgramare,String date,String hour,Animal Animal,Medic Medic) {
		this.idProgramare=idProgramare;
		this.date=date;
		this.hour=hour;
		this.animal=Animal;
		this.medic=Medic;
	}
	//CREATE
	public void Create(int idProgramare,String date,String hour,Animal Animal,Medic Medic) {
		this.idProgramare=idProgramare;
		this.date=date;
		this.hour=hour;
		this.animal=Animal;
		this.medic=Medic;
	}

	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHour() {
		return this.hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Medic getMedic() {
		return this.medic;
	}

	public void setMedic(Medic medic) {
		this.medic = medic;
	}

}