package util;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Medic;
import model.Programare;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp() throws Exception
	{
		entityManagerFactory =Persistence.createEntityManagerFactory("AppCabinetVeterinarJPA");
		entityManager=entityManagerFactory.createEntityManager();
		
	}
	//save entities
	public void saveAnimal(Animal animal)
	{
		entityManager.persist(animal);
	}
	public void saveMedic(Medic  medic)
	{
		entityManager.persist(medic);
	}
	public void saveProgramare(Programare programare)
	{
		entityManager.persist(programare);
	}
	
	
	
	public void startTransaction()
	{
		entityManager.getTransaction().begin();
	}
	public void commitTransaction()
	{
		entityManager.getTransaction().commit();
	}
	public void closeEntityManager()
	{
		entityManager.close();
	}
	
	//Print entitities
	public void printAllAnimalsFromDB()
	{
		List<Animal> results =entityManager.createNativeQuery("Select * from appcabinet.Animal", Animal.class).getResultList();
		for(Animal animal : results)
		{
			System.out.println("IdAnimal: "+animal.getIdAnimal()+" Name: "+animal.getName()+" Species: "+animal.getSpecies()
			+" Age:  "+animal.getAge()+" Weight: "+animal.getWeight()+" Owner Name: "+ animal.getOwnerName());
		}
	}
	
	public void printAllMedicsFromDB()
	{
		List<Medic> results =entityManager.createNativeQuery("Select * from appcabinet.Medic", Medic.class).getResultList();
		for(Medic medic : results)
		{
			System.out.println("IdMedic: "+medic.getIdMedic()+" Name: "+medic.getName());
		}
	}
	
	public void printAllProgramaresFromDb() {
		List<Programare> results = entityManager
				.createNativeQuery("Select * from appcabinet.Programare", Programare.class).getResultList();
		for (Programare programare : results) {
			System.out.println("IdProgramare: "+programare.getIdProgramare()+" Date: "+programare.getDate()+" Hour:"
					+programare.getHour()+" with Animal:"+programare.getAnimal().getName()+" with Medic: "
					+programare.getMedic().getName());
		}
	}
	
	/**
	 *DELETE ANIMAL
	 * @param tool
	 */
	public void deleteAnimal(int tool) {
		Animal animal = entityManager.find(Animal.class, tool);
		entityManager.getTransaction().begin();
		entityManager.remove(animal);
		entityManager.getTransaction().commit();

	}
	
	/**
	 * DELETE MEDIC
	 * @param tool
	 */
	public void deleteMedic(int tool)
	{
		Medic medic=entityManager.find(Medic.class, tool);
		entityManager.getTransaction().begin();
		entityManager.remove(medic);
		entityManager.getTransaction().commit();
	}

	/**
	 * DELETE PROGRAMARE
	 * @param tool
	 */
	public void deleteProgramare(int tool)
	{
		Programare programare=entityManager.find(Programare.class, tool);
		entityManager.getTransaction().begin();
		entityManager.remove(programare);
		entityManager.getTransaction().commit();
	}
	
	/**
	 * UPDATE ANIMAL
	 * @param id,name,Age,Weight,Species,OwnerName
	 */
	public void updateAnimal(int id,String name,int Age,float Weight,String Species,String OwnerName) {
		Animal animal = entityManager.find(Animal.class, id);
		entityManager.getTransaction().begin();
		//animal.setIdAnimal(id);
		animal.setName(name);
		animal.setAge(Age);
		animal.setSpecies(Species);
		animal.setWeight(Weight);
		animal.setOwnerName(OwnerName);
		
		entityManager.getTransaction().commit();

	}
	
	/**
	 * UPDATE Medic
	 * @param id,name
	 */
	public void updateMedic(int id,String name) {
		Medic medic = entityManager.find(Medic.class, id);
		entityManager.getTransaction().begin();
		medic.setIdMedic(id);
		medic.setName(name);		
		entityManager.getTransaction().commit();
	}
	
	/**
	 * UPDATE PROGRAMARE
	 * @param int idProgramare,Date date,Time hour,Animal Animal,Medic Medic
	 */
	public void updateProgramare(int idProgramare,String date,String hour,Animal Animal,Medic Medic) {
		Programare programare = entityManager.find(Programare.class, idProgramare);
		entityManager.getTransaction().begin();
		programare.setIdProgramare(idProgramare);
		programare.setDate(date);
		programare.setHour(hour);
		programare.setAnimal(Animal);
		programare.setMedic(Medic);		
		entityManager.getTransaction().commit();
	}
	
	public List<Animal> animalList()
	{
		List<Animal> animalList=(List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}
	
	
	
}
