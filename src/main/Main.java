package main;
//import java.util.Date;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Medic;
import model.Programare;
import util.DatabaseUtil;

public class Main extends Application{

//	public static void main(String[] args) throws Exception {
//		DatabaseUtil dbUtil =new DatabaseUtil();
//		//Insert Animal
//		Animal dog1=new Animal();
//		dog1.Create(6, "Dog6", 6, 60, "Pitbull6", "Owner6");
//		Medic medic1=new Medic();
//		medic1.Create(6, "Medic6");
//		Programare programare1=new Programare();
//		programare1.Create(6, "2018-04-22","12:00:00", dog1, medic1);
//		
//		
//		
//		dbUtil.setUp();
//		dbUtil.startTransaction();
//		dbUtil.saveAnimal(dog1);
//		dbUtil.saveMedic(medic1);
//		dbUtil.saveProgramare(programare1);
//		//dbUtil.deleteAnimal(1);
//		//dbUtil.updateAnimal(1, "dog1", 1, 5, "Pitbull", "Owner1");
//		dbUtil.commitTransaction();
//		dbUtil.printAllAnimalsFromDB();
//		dbUtil.printAllMedicsFromDB();
//		dbUtil.printAllProgramaresFromDb();
//	//	dbUtil.deleteProgramare(4);
//	//	dbUtil.deleteAnimal(3);
//	//	dbUtil.deleteMedic(3);
//		
//		dbUtil.closeEntityManager();
//		
//		
//
//	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			BorderPane root1=(BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene1 = new Scene(root1,800,800);
			primaryStage.setScene(scene1);
			primaryStage.show();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}

}
